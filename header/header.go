package header

import (
	"fmt"
)

type Header struct {
	Cmd string `json:"cmd"`
}

func (h Header) Ok() bool {
	return h.Cmd != ""
}

func (h Header) String() string {
	return fmt.Sprintf("mocked proto cmd=%s", h.Cmd)
}
