package echo

import (
	"context"
	"fmt"
)

func NewProcessor(text string) func(context.Context, any) ([]byte, error) {
	return func(context.Context, any) ([]byte, error) {
		resp := fmt.Sprintf(`{"response":"%s"}`, text)
		return []byte(resp), nil
	}
}
