package auth

import (
	"context"
	"encoding/json"
	"errors"

	"gitlab.com/vpirogov/hardrockproto/header"
	"gitlab.com/vpirogov/jsonproto"
)

var (
	Authenticated bool

	ErrInvalidHeader = errors.New("auth: invalid header")
	ErrInvalidInput  = errors.New("auth: invalid input")
	ErrInvalidAction = errors.New("auth: invalid action")
)

type AuthRequest struct {
	Action   string
	Username string
	Password string
}

func Process(ctx context.Context, input any) ([]byte, error) {
	msg, ok := input.(jsonproto.Message[header.Header])
	if !ok {
		return nil, ErrInvalidHeader
	}

	var ar AuthRequest
	err := json.Unmarshal(msg.Body(), &ar)
	if err != nil {
		return nil, ErrInvalidInput
	}

	return process(ar)
}

func process(ar AuthRequest) ([]byte, error) {
	switch ar.Action {
	case "login":
		return login(ar.Username, ar.Password)
	case "logout":
		return logout()
	case "get status":
		if Authenticated {
			return []byte(`{"status":"logged in"}`), nil
		}
		return []byte(`{"status":"nobody home"}`), nil
	}

	return nil, ErrInvalidAction
}

func login(u string, p string) ([]byte, error) {
	if Authenticated {
		return []byte(`{"error":"allready authenticated"}`), nil
	}
	if u == "hardrock listener" && p == "let me in" {
		Authenticated = true
		return []byte(`{"message":"authenticated and authorised"}`), nil
	}
	return []byte(`{"message":"invalid credentials"}`), nil
}

func logout() ([]byte, error) {
	if !Authenticated {
		return []byte(`{"error":"not logged in"}`), nil
	}
	Authenticated = false
	return []byte(`{"message":"see you"}`), nil
}
