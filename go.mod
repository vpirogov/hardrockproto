module gitlab.com/vpirogov/hardrockproto

go 1.19

require gitlab.com/vpirogov/jsonproto v0.5.5

require github.com/gorilla/websocket v1.5.0 // indirect
