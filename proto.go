package hardrockproto

import (
	"gitlab.com/vpirogov/hardrockproto/auth"
	"gitlab.com/vpirogov/hardrockproto/echo"
	"gitlab.com/vpirogov/hardrockproto/header"
	"gitlab.com/vpirogov/hardrockproto/search"

	"gitlab.com/vpirogov/jsonproto"
)

const echoTextDI = "still alive..."

func NewHandler() *jsonproto.Router {
	r := jsonproto.NewRouter()
	r.Add(NewMatcher("ping"), echo.NewProcessor(echoTextDI))
	r.Add(NewMatcher("search"), search.Process)
	r.Add(NewMatcher("auth"), auth.Process)
	return r
}

func NewMatcher(cmd string) func(any) bool {
	return func(input any) bool {
		h, ok := input.(jsonproto.Message[header.Header])
		if !ok {
			return false
		}
		return h.Header().Cmd == cmd
	}
}
