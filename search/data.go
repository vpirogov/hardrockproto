package search

var jsondata = []byte(`[
  {
    "album": "High Voltage (Australia)",
    "year": 1975,
    "label": "Albert"
  },
  {
    "album": "T.N.T. (Australia)",
    "year": 1975,
    "label": "Albert"
  },
  {
    "album": "High Voltage (International)",
    "year": 1976,
    "label": "Atlantic"
  },
  {
    "album": "Dirty Deeds Done Dirt Cheap",
    "year": 1976,
    "label": "Albert/Atlantic"
  },
  {
    "album": "Let There Be Rock",
    "year": 1977,
    "label": "Albert/Atlantic"
  },
  {
    "album": "Let There Be Rock",
    "year": 1978,
    "label": "Atlantic"
  },
  {
    "album": "Highway to Hell",
    "year": 1979,
    "label": "Atlantic"
  },
  {
    "album": "Back in Black",
    "year": 1980,
    "label": "Atlantic"
  },
  {
    "album": "For Those About to Rock We Salute You",
    "year": 1981,
    "label": "Atlantic"
  },
  {
    "album": "Flick of the Switch",
    "year": 1983,
    "label": "Atlantic"
  },
  {
    "album": "Fly on the Wall",
    "year": 1985,
    "label": "Atlantic"
  },
  {
    "album": "Blow Up Your Video",
    "year": 1988,
    "label": "Atlantic"
  },
  {
    "album": "The Razors Edge",
    "year": 1990,
    "label": "Atlantic"
  },
  {
    "album": "Ballbreaker",
    "year": 1995,
    "label": "East West"
  },
  {
    "album": "Stiff Upper Lip",
    "year": 2000,
    "label": "East West"
  },
  {
    "album": "Black Ice",
    "year": 2008,
    "label": "Columbia"
  },
  {
    "album": "Rock or Bust",
    "year": 2014,
    "label": "Albert/Columbia"
  },
  {
    "album": "Power Up",
    "year": 2020,
    "label": "Columbia"
  }
]
`)
