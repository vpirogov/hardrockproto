package search

import (
	"context"
	"encoding/json"
	"errors"

	"gitlab.com/vpirogov/hardrockproto/header"
	"gitlab.com/vpirogov/jsonproto"
)

var (
	ErrInvalidHeader = errors.New("search: invalid header")
	ErrInvalidInput  = errors.New("search: invalid input")
)

type Query struct {
	Cmd    string `json:"cmd"`
	Year   int32  `json:"year"`
	Before int32  `json:"before"`
	After  int32  `json:"after"`
}

type Album struct {
	Album string `json:"album"`
	Year  int32  `json:"year"`
	Label string `json:"label"`
}

func Process(ctx context.Context, input any) ([]byte, error) {
	msg, ok := input.(jsonproto.Message[header.Header])
	if !ok {
		return nil, ErrInvalidHeader
	}

	var q Query
	err := json.Unmarshal(msg.Body(), &q)
	if err != nil {
		return nil, ErrInvalidInput
	}

	return search(q)
}

func search(q Query) ([]byte, error) {
	albumz, err := load()
	if err != nil {
		return nil, err
	}

	if q.Year > 0 {
		for _, album := range albumz {
			if album.Year == q.Year {
				return json.Marshal(album)
			}
		}
	}

	var resp struct {
		Albums []Album `json:"albums"`
		Count  int     `json:"count"`
	}
	resp.Albums = make([]Album, 0)

	for _, album := range albumz {
		gonnaAdd := true
		if q.After > 0 && album.Year < q.After {
			gonnaAdd = false
		}
		if q.Before > 0 && album.Year > q.Before {
			gonnaAdd = false
		}

		if gonnaAdd {
			resp.Albums = append(resp.Albums, album)
			resp.Count++
		}
	}

	return json.Marshal(resp)
}

func load() ([]Album, error) {
	albums := make([]Album, 1, 15)
	err := json.Unmarshal(jsondata, &albums)
	if err != nil {
		return nil, err
	}

	return albums, nil
}
